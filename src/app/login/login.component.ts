import { Component, Inject, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { LOCAL_STORAGE, StorageService } from 'angular-webstorage-service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})

export class LoginComponent {
  message = 'Logged out';
  @ViewChild('pass') username: ElementRef;
  @ViewChild('user') password: ElementRef;

  constructor(
    public authService: AuthService,
    public router: Router,
    @Inject(LOCAL_STORAGE) private storage: StorageService
  ) {
    if (this.storage.get('user')) {
      this.router.navigate(['/invoices']);
    }
  }

  setMessage() {
    this.message = 'Authenticating...';
  }

  login() {
    this.setMessage();
    this.authService.login(this.username.nativeElement.value, this.password.nativeElement.value)
      .subscribe( (resp) => {
        if (resp) {
          this.storage.set('user', resp);
          this.router.navigate(['/invoices']);
      }
    }, (err) => {
        this.message = err.error.errorMessage;
      });
  }

  logout() {
    this.authService.logout();
    this.message = 'Logged out';
  }
}
