import { Injectable, Inject } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { LOCAL_STORAGE, StorageService } from 'angular-webstorage-service';

@Injectable()

export class AuthService implements CanActivate {
  redirectUrl: string;
  backendUrl = 'http://localhost:5000';
  invoices;
  message;

  constructor(
    private router: Router,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private http: HttpClient
  ) {}

  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      const url: string = state.url;
      return this.checkLogin(url);
    }

    checkLogin(url: string): boolean {
      if (this.storage.get('user')) {
        return true;
      }

      this.redirectUrl = url;

      this.router.navigate(['/login']);

      return false;
    }

    login(username, password) {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded',
        })
      };

      const body = `user=${username}&password=${password}`;

      return this.http.post(this.backendUrl + '/login', body, httpOptions).pipe(
        map( res => res )
      );
    }

  logout(): void {
    this.storage.remove('user');
    this.router.navigate(['/login']);
  }
}
