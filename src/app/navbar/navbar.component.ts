import { Component, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'angular-webstorage-service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [LoginComponent]
})

export class NavbarComponent {
  constructor(public log: LoginComponent, @Inject(LOCAL_STORAGE) private storage: StorageService) { }

  logout() {
    this.log.logout();
  }
}
