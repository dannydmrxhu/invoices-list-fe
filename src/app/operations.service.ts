import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LOCAL_STORAGE, StorageService } from 'angular-webstorage-service';
import { map } from 'rxjs/operators';
@Injectable()

export class OperationsService {
  redirectUrl: string;
  backendUrl = 'http://localhost:5000';
  invoices;

  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private http: HttpClient
  ) {}

  addInvoice(invoice) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.storage.get('user').token
      })
    };

      return this.http.post(this.backendUrl + '/invoices', invoice, httpOptions)
        .pipe(
            map(res => res)
        );
  }

  getInvoices() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.storage.get('user').token
      })
    };

    return this.http.get(this.backendUrl + '/invoices', httpOptions)
    .pipe(
      map(res => res)
    );
  }

  removeInvoice(invoice) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.storage.get('user').token
      })
    };

      const id = invoice.id;
      return this.http.delete(this.backendUrl + '/invoices/' + id, httpOptions)
        .pipe(
            map( res => res )
        );
  }

  editInvoice(invoice) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.storage.get('user').token
      })
    };

      return this.http.put(this.backendUrl + '/invoices', invoice, httpOptions)
        .pipe(
            map( res => res )
        );
  }
}
