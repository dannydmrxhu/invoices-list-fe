import { Component, Inject, ViewChild, Input, Output, EventEmitter, OnChanges, ElementRef, AfterViewInit } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'angular-webstorage-service';
import { debounceTime, map } from 'rxjs/operators';
import { fromEvent } from 'rxjs';
import { AuthService } from '../auth.service';
import { OperationsService } from '../operations.service';

@Component({
  selector: 'app-invoices-list',
  templateUrl: './invoices-list.component.html',
  styleUrls: ['./invoices-list.component.css'],
  providers: [AuthService, OperationsService]
})

export class InvoicesListComponent implements AfterViewInit, OnChanges {
  // Declare two arrays of objects that are initialized with the same values
  // one for displaying the invoices depending on the needs of the user and
  // the other for restoring the original values

  invoices;
  safeCopy;
  @ViewChild('search') input: ElementRef;
  @Input() newInvoice;
  @Input() copyInvoice;
  @Input() removeInvoice;
  @Input() editInvoice;
  @Output() invoiceArray = new EventEmitter();
  @Output() clearItems = new EventEmitter(true);

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService, private operationsService: OperationsService) {

  }

  ngOnChanges() {
    if (this.newInvoice !== null) {
      this.add(this.newInvoice);
      this.clearItems.emit();
    }

    if (this.copyInvoice !== null) {
      this.copyItem(this.copyInvoice);
      this.clearItems.emit();
    }

    if (this.removeInvoice !== null) {
      this.removeItem(this.removeInvoice);
      this.clearItems.emit();
    }

    if (this.editInvoice != null) {
      this.editItem(this.editInvoice);
      this.clearItems.emit();
    }

    this.emitInvoices();
  }

  ngAfterViewInit() {

    this.operationsService.getInvoices().subscribe( invoices => {
      this.invoices = invoices;
      this.safeCopy = this.invoices.slice(0);
      this.emitInvoices();
    });

    const search = fromEvent(this.input.nativeElement, 'keyup');

    const item = search.pipe(map( (event: any) => event.target.value));

    const debouncedInput = item.pipe(debounceTime(200));

    const sub = debouncedInput.subscribe( val => {
      return this.searchItems(val);
    });
  }

  add(newInvoice) {
    this.operationsService.addInvoice(newInvoice).subscribe( (invoice) => {
      this.invoices.unshift(invoice);
      this.safeCopy.unshift(invoice);
      this.emitInvoices();
    });
  }

  removeItem(item) {

    this.operationsService.removeInvoice(item).subscribe( (invoices) => {
      const pos = this.invoices.indexOf(item);
      this.invoices.splice(pos, 1);
      this.safeCopy = invoices;
      this.emitInvoices();
    });
  }


  copyItem(item) {
    const newItem = item.name.includes('Copy') ? {name: item.name} : {name: item.name + ' (Copy)'};
    this.operationsService.addInvoice(newItem).subscribe( invoice => {
      this.invoices.unshift(invoice);
      this.safeCopy.unshift(invoice);
      this.emitInvoices();
    });

  }

  editItem(item) {

    this.operationsService.editInvoice(item).subscribe( invoices => {
      this.changeName(this.invoices, item);
      this.safeCopy = invoices;
      this.emitInvoices();
    });
  }

  emitInvoices() {
    this.invoiceArray.emit(this.invoices);
  }

  changeName(array, obj) {
    for (let i = 0; i < this.invoices.length; i++) {
      if (array[i].id === obj.id) {
        array[i].name = obj.name;
      }
    }
  }

  searchItems(text: string) {
    // Restore the original array to make sure we don't search items
    // that are already searched or displayed
    this.invoices = this.safeCopy.slice(0);

    if (text !== '') {

      const numItems: number = this.invoices.length;
      const searched = [];

      for (let i = 0; i < numItems; i++) {
        // Search for the items case insensitive
        if (this.invoices[i].name.toLowerCase().includes(text.toLowerCase())) {
          searched.push(this.invoices[i]);
        }
      }
      // Copy the searched items to the invoices with is used to display them
      this.invoices = searched.splice(0);
    }

    this.emitInvoices();
  }
}
