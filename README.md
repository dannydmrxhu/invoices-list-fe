# Angular Invoices List #

This project displays a list of invoices that a user has obtained. The application consists of two pages: the account page where the user logs in and the invoices page where the user is displayed with the list of invoices. The user can add a new invoice, edit each invoice by clicking on the name of each invoice, save any changes, delete invoices and even copy them. Furthermore, the user can search for a particular invoice and can make changes whenever possible. 

The accounts page is still in development, but anybody can log in using <admin> as the username and password to test this applications features. 

The Invoices List application also has the backend portion integrated where the backend server is built using express and node js.

Use git clone https://dannydmrxhu@bitbucket.org/dannydmrxhu/invoices-list-be.git to obtain the backend portion of this application.

## Development server ##

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

